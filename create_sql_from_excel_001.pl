use strict;
use Data::Dumper;
#use Switch;
use Spreadsheet::XLSX;
#use Text::Iconv;
$| = 1; #don't buffer output


#my $converter = Text::Iconv->new("utf-8", "windows-1251");
my $excel = Spreadsheet::XLSX->new('DB_Export_Daten_Eva_Appli.xlsx');

my @image = ();

my $jahr = {};
my $ort = {};
my $standort = {};
my $technikthesaurus = {};
my $technik = {};
my $image_has_technik = {};

my $sheet = @{$excel->{Worksheet}}[0];
#print "open sheet: $sheet->{Name}\n";
foreach my $row ($sheet->{MinRow} .. $sheet->{MaxRow}){
    if($row != 0){
	my $entry = {};
	foreach my $col ($sheet->{MinCol} .. $sheet->{MaxCol}){ 
	    my $name  = $sheet->{Cells}[0][$col]->{Val};
	    my $value = $sheet->{Cells}[$row][$col]->{Val};   
	    $entry->{$name} = $value;	     
	}	
	push(@image,$entry);
	$entry->{"ID"} = scalar(@image)-1;
	setupData($entry);
    }   
}

my $stmnt = "";
$stmnt .= createStorageInsertStatement('jahr', $jahr, 0);
$stmnt .= createStorageInsertStatement('ort', $ort, 1);
$stmnt .= createStorageInsertStatement('standort', $standort, 1);
$stmnt .= createStorageInsertStatement('technikthesaurus', $technikthesaurus, 1);
$stmnt .= createStorageInsertStatement('technik', $technik, 1);
$stmnt .= createNMInsertStatement('image_has_technik', $image_has_technik);
$stmnt .= createImageInsertStatement();

open( SQL, ">insertData.sql" );
print SQL $stmnt;
close( SQL );



sub createImageInsertStatement{
    my $table = "image";
    my $sql = "";

    foreach(@image){
	if( defined $_->{"FILENAME"} ){	
	    $sql .= "INSERT INTO $table VALUES( ";
	    $sql .= cleanValue($_->{"ID"}).", ";
	    $sql .= cleanText(cleanValue($_->{"SIK_INVNR"})).", ";
	    $sql .= cleanText(cleanValue($_->{"KAT_NR"})).", ";
	    $sql .= cleanText(cleanValue($_->{"TITEL"})).", ";
	    $sql .= cleanText(cleanValue($_->{"DATIERUNG"})).", ";
	    $sql .= cleanText(cleanValue($_->{"TECHNIK_TEXT"})).", ";
	    $sql .= cleanText(cleanValue($_->{"MASSE_TEXT"})).", ";
	    $sql .= cleanValue($_->{"MASSE_BREITE"}).", ";
	    $sql .= cleanValue($_->{"MASSE_LAENGE"}).", ";
	    $sql .= cleanValue($_->{"MASSE_HOEHE"}).", ";
	    $sql .= cleanText(cleanValue($_->{"STANDORT_TEXT"})).", ";
	    $sql .= cleanValue($_->{"FK_STANDORT"}).", ";
	    $sql .= cleanValue($_->{"FK_ORT"}).", ";
	    $sql .= cleanText(cleanValue($_->{"STANDORT_URL"})).", ";
	    $sql .= cleanText(cleanValue($_->{"FILENAME"})).", ";
	    $sql .= cleanText(cleanValue($_->{"REPRO_NACHWEIS"})).", ";
	    $sql .= cleanValue($_->{"FK_JAHR"}).", ";
	    $sql .= cleanValue($_->{"FK_TECHNIKTHESAURUS"}).", ";
	    $sql .= cleanText(cleanValue($_->{"COPYRIGHT"})).", ";
	    $sql .= cleanText(cleanValue($_->{"SIKART_URL"}));
	    $sql .= " );\n";
	}
    }

    return $sql;
}

sub cleanValue{
    my $value = shift;
    if( ! defined $value ){
	$value = "NULL";
    }else{
	$value =~ s/(\d+),(\d+)/$1\.$2/;
    }
    return $value;
}

sub cleanText{
    my $text = shift;
    if($text ne "NULL"){
	$text =~ s/'/`/g;
	chomp($text);    
	return "'$text'";
    }
    return $text;
    
}

sub createNMInsertStatement{
    my $table = shift;
    my $nm = shift;

    my $sql = "";

    foreach my $key( sort keys %{$nm} ){

	my @values = @{$nm->{$key}};
	foreach my $value (@values){
	    $sql .= "INSERT INTO $table VALUES( $key, $value );\n";;
	}	
    }

    return $sql;
}

sub createStorageInsertStatement{
    my $table = shift;
    my $storage = shift;
    my $keyIsText = shift;
    
    my $sql = "";

    #print "$table\n";
    foreach my $key( sort keys %{$storage} ){
	#print " k:$key\n";
	if( $key ne "increment" ){
	    my $value = $storage->{$key};
	    #print " v:$value\n";
	    if($keyIsText){
		$key = cleanText($key);		
	    }
	    $sql .= "INSERT INTO $table VALUES( $value, $key );\n";
	}	
    }

    #print $sql;
    return $sql;

}

sub setupData{
    my $entry = shift;

    
    if( defined $entry->{"JAHR"}){
	$entry->{"FK_JAHR"} = addEntry( $jahr,$entry->{"JAHR"} );
    }
    
    if( defined $entry->{"ORT"}){
	$entry->{"FK_ORT"} = addEntry( $ort,$entry->{"ORT"} );
    }
    if( defined $entry->{"STANDORT"}){
	my @tmp = split(/,/,$entry->{"STANDORT"});
	$entry->{"FK_STANDORT"} = addEntry( $standort, $tmp[0] );
	$entry->{"STANDORT_TEXT"} = $entry->{"STANDORT"};
    }
    if( defined $entry->{"TECHNIKTHESAURUS"}){
	$entry->{"FK_TECHNIKTHESAURUS"} = addEntry( $technikthesaurus, $entry->{"TECHNIKTHESAURUS"} );
    }
    if( defined $entry->{"TECHNIK"}){
	$entry->{"TECHNIK_TEXT"} = $entry->{"TECHNIK"};
	my @tmp = split(/,/,$entry->{"TECHNIK"});
	my @ids = ();
	foreach(@tmp){
	    #chomp($_);
	    $_ =~ s/^\s+|\s+$//g;
	    #print "'$_'\n";
	    push(@ids,addEntry( $technik, $_ ));	    
	}
	$image_has_technik->{ $entry->{"ID"} } = \@ids;
    }

    if( defined $entry->{"MASSE"} ){
	$entry->{"MASSE_TEXT"} = $entry->{"MASSE"};
	if(  $entry->{"MASSE"} ne "Masse unbekannt" ){
	    if( $entry->{"MASSE"} =~ m/^(\d+,?\d*)\s?(cm)?\s?hoch.*?/ ){
		#print $entry->{"MASSE"}." -> "."$1\n";		
		$entry->{"MASSE_HOEHE"} = $1;
	    }elsif( $entry->{"MASSE"} =~ m/^(\d+,?\d*)\s?x\s?(\d+,?\d*)\s?cm$/ ){
		#print $entry->{"MASSE"}." -> "."$1 $2 \n";
		my $l = ($1 > $2)? $1 : $2;
		my $b = ($1 < $2)? $1 : $2;
		$entry->{"MASSE_LAENGE"} = $l;
		$entry->{"MASSE_BREITE"} = $b;
	    }elsif( $entry->{"MASSE"} =~ m/^(\d+,?\d*)\s?x\s?(\d+,?\d*)\s?x\s?(\d+,?\d*)\s?cm$/ ){
		#print $entry->{"MASSE"}." -> "."$1 $2 $3 \n";
		my $l = ($1 > $2)? $1 : $2;
		my $b = ($1 < $2)? $1 : $2;
		$entry->{"MASSE_LAENGE"} = $l;
		$entry->{"MASSE_BREITE"} = $b;
		$entry->{"MASSE_HOEHE"}  = $3;
	    }

	  

	    
	}
    }
    
}

sub addEntry{
    my $storage = shift;
    my $value   = shift;

    if( ! defined $storage->{$value} ){
	if( ! defined $storage->{"increment"} ){
	    $storage->{"increment"} = 0;
	}
	$storage->{$value} = $storage->{"increment"};
	$storage->{"increment"}++;
    }

    return $storage->{$value};
    
}



#print Dumper($image_has_technik);




