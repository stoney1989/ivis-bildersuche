<html>
    <head>
        <script src="js/babylon.js"></script>
        <script src="js/jquery-2.1.3.min.js"></script>
        <script src="js/hand.minified-1.3.8.js"></script>


        <link rel="stylesheet" type="text/css" href="css/bildersuche.css"/>
    </head>
    <body>
        <canvas id="renderCanvas"></canvas>

        <div id="loadingScreen">
            <img src="icon/loading.gif"/>
            <br/>
            <span>loading ...</span>
        </div>

        <div id="show">
            <img id="show_image"/>
            <table>
                <tr>
                    <td><b>Titel:</b></td>
                    <td class="show_info" id="show_titel">-</td>
                </tr>
                <tr>
                    <td><b>Sik Invnr:</b></td>
                    <td class="show_info" id="show_sik_invnr">-</td>
                </tr>
                <tr>
                    <td><b>Katalognummer:</b></td>
                    <td class="show_info" id="show_kat_nr">-</td>
                </tr>
                <tr>
                    <td><b>Datierung:</b></td>
                    <td class="show_info" id="show_datierung">-</td>
                </tr>
                <tr>
                    <td><b>Technik:</b></td>
                    <td class="show_info" id="show_technik">-</td>
                </tr>
                <tr>
                    <td><b>Masse:</b></td>
                    <td class="show_info" id="show_masse">-</td>
                </tr>
            </table>
        </div>

        <div id="menu">
            <img class="buttons" id="button_fullscreen" src="icon/full.png"/>
            <img class="buttons" id="button_add"        src="icon/plus.png"/>

            <select id="select_order">
                <option value="none"> ---- </option>
                <option value="order_by_reference=jahr"> Jahr </option>
                <option value="order_by=titel"> Titel </option>
                <option value="order_by_reference=ort"> Ort </option>
                <option value="order_by_reference=standort"> Standort </option>
                <option value="order_by_reference=technikthesaurus"> Technikthesaurus </option>
            </select>
        </div>



        <script>
            var canvas = document.getElementById('renderCanvas');
            var engine = new BABYLON.Engine(canvas,true);

            var imageSpriteManager;
            var sprites = [];


            var camera;
            var scene = createScene();

            var DIMENSION = 128;
            var IMAGES_PER_LINE = 32;

            //data storage
            var images = [];

            var blockInput = false;
            var orderBy = '';



            //on page loaded
            $(function(){
                $('#button_fullscreen').click(function(event){
                    event.stopPropagation();
                    requestFullScreen(document.body);
                });

                $('#select_order').click(function(event){
                    event.stopPropagation();
                });

                $('#button_add').click(function(event){
                    event.stopPropagation();
                    $('body').append( createSearchFilter() );
                });

                setupOrder();
                getData();

            });

            function setupOrder(){
                $('#select_order').change(function(){
                    var data = $(this).find("option:selected").attr('value');
                    if( data !== 'none'){
                        orderBy = data;
                    }else{
                        orderBy = '';
                    }
                    useSearchFilter();
                });
            }

            function createScene(){
                var scene = new BABYLON.Scene(engine);


                camera = new BABYLON.FreeCamera("FreeCamera", new BABYLON.Vector3(0, 0, 0), scene);
                camera.position.x = -30;
                camera.position.y = -10;
                camera.position.z = -20;

//                camera.mode = BABYLON.Camera.ORTHOGRAPHIC_CAMERA;
//
//                camera.orthoTop = 20;
//                camera.orthoBottom = -20;
//                camera.orthoLeft = -20;
//                camera.orthoRight = 20;

                setupCameraControl( camera );

                camera.setTarget(BABYLON.Vector3.Zero());
                camera.attachControl(canvas, true);

                camera.speed = 1;

                var ball = BABYLON.Mesh.CreateSphere("ball", 100,3.0, scene);
                ball.material = new BABYLON.StandardMaterial("ball", scene);
                ball.position.z = -1;


                var r =   Math.sin(0) * (127) + 128;
                var g =   Math.sin(2) * (127) + 128;
                var b =   Math.sin(4) * (127) + 128;

                ball.material.emissiveColor = new BABYLON.Color3( Math.floor(r)/255,Math.floor(g)/255,Math.floor(b)/255)


                scene.clearColor = new BABYLON.Color3(1,1,1);
                return scene;
            };


            //When click event is raised
            window.addEventListener("click", function () {
                // We try to pick an object
                $('#show').css('display','none');
                var pickResult = scene.pick(scene.pointerX, scene.pointerY);

                if(pickResult !== undefined && pickResult['hit']){

                    if( pickResult['pickedMesh']['image_index'] === undefined ){
                        camera.position.x = 0;
                        camera.position.y = 0;
                        camera.position.z = -30;
                        camera.setTarget( pickResult['pickedMesh'].absolutePosition );

                    }else{


                        var image = images[pickResult['pickedMesh']['image_index']];

                        console.log(image);

                        $('#show_image').attr('src','image/'+image['FILENAME']+'.jpg')
                        $('#show_titel').text( image['TITEL'] );
                        $('#show_sik_invnr').text( image['SIK_INVNR'] );
                        $('#show_kat_nr').text( image['KAT_NR'] );
                        $('#show_datierung').text( image['DATIERUNG'] );
                        $('#show_technik').text( image['TECHNIK_TEXT'] );
                        $('#show_masse').text( image['MASSE_TEXT'] );
                        $('#show').css('display','block');
                    }


                }

            });




            function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }

            function setupCameraControl( camera ){
                //hijack teh camera ...
                camera.keysLeft = [37,33,34,65,81,69];
                camera.keysRight = [39,68];
                camera.keysDown = [40,83];
                camera.keysUp = [38,87];
                camera._checkInputs = function () {
                    if(!blockInput){
                        if (!this._localDirection) {
                            this._localDirection = BABYLON.Vector3.Zero();
                            this._transformedDirection = BABYLON.Vector3.Zero();
                        }

                        for (var index = 0; index < this._keys.length; index++) {
                            var keyCode = this._keys[index];
                            var speed = this._computeLocalCameraSpeed();
                            if (this.keysLeft.indexOf(keyCode) !== -1 && keyCode != 33 && keyCode != 34 && keyCode != 81 && keyCode != 69) {
                                this._localDirection.copyFromFloats(-speed, 0, 0);
                            }
                            else if (this.keysUp.indexOf(keyCode) !== -1) {
                                this._localDirection.copyFromFloats(0, speed, 0);
                            }
                            else if (this.keysRight.indexOf(keyCode) !== -1) {
                                this._localDirection.copyFromFloats(speed, 0, 0);
                            }
                            else if (this.keysDown.indexOf(keyCode) !== -1) {
                                this._localDirection.copyFromFloats(0, -speed, 0);
                            }else if( keyCode == 33 || keyCode == 81){
                                this._localDirection.copyFromFloats(0, 0, speed);
                            }else if( keyCode == 34 || keyCode == 69){
                                this._localDirection.copyFromFloats(0, 0, -speed);
                            }

                            this.getViewMatrix().invertToRef(this._cameraTransformMatrix);
                            BABYLON.Vector3.TransformNormalToRef(this._localDirection, this._cameraTransformMatrix, this._transformedDirection);
                            this.cameraDirection.addInPlace(this._transformedDirection);
                        }
                    }

                };

            }


            function disposeImageData(){
                for(var i = 0; i < images.length; i++){
                    if( images[i]['image_object'] !== undefined ){
                        if(images[i]['image_object']['hit'] !== undefined)images[i]['image_object']['hit'].dispose();
                        images[i]['image_object'].dispose();
                    }
                }
            }

            function loadImageData(){
                //dummy loading time...
                if(imageSpriteManager === undefined)imageSpriteManager = new BABYLON.SpriteManager("imageManager", 'getImageCanvas.php', 500, DIMENSION, scene);

                setTimeout(function(){
                    //setupGlobalCircle();
                    setupGlobalSpiral()
                }, getRandomInt(300,400));
            }

            function createImageObject( i, x, y, z, scale, angle, color ){
                var image = new BABYLON.Sprite("image"+i, imageSpriteManager);
                image.size = scale;
                image.angle = angle;
                var hit = BABYLON.Mesh.CreateSphere("sphere", 10, image.size*1.4, scene);
                hit.material = new BABYLON.StandardMaterial("texture1", scene);
                hit.material.alpha = 0.5;
                //console.log(color);
                hit.material.emissiveColor = color;
                //hit.material.diffuseColor = new BABYLON.Color3(0.5,0.5,0.5);


                hit['image_index'] = i;
                hit.isPickable = true;
                image['hit'] = hit;

                //cell index of texture atlas
                image['cellIndex'] = images[i]['ID'];

                image.position.x = x;
                image.position.y = y;
                image.position.z = z;

                hit.position.x = image.position.x;
                hit.position.y = image.position.y;
                hit.position.z = image.position.z;

                images[i]['image_object'] = image;


                return image;
            }

            function setupGlobalCircle(){
                var r = 0;
                var putCount = -1;
                var nextPutCount = 7;
                var rot = 0;
                for (var i=0; i < images.length && i < DIMENSION*DIMENSION; i++) {
                    if( putCount == -1 ){
                        putCount = nextPutCount;
                        nextPutCount += 8;
                        r += 2.5;
                        rot = (Math.PI*2)/(putCount+1);
                    }

                    var angle = rot*putCount;
                    var x = 0;
                    var y = r;

                    createImageObject(i,Math.cos(angle)*x - Math.sin(angle)*y,Math.sin(angle)*x + Math.cos(angle)*y,-1, getRandomInt(8,12)/10, getRandomInt(-100,100)/10 );
                    putCount--;
                }
                $('#loadingScreen').css('display','none');
            }

            function setupGlobalSpiral(){

                var d = 1.5;

                var angle = 0;



                //var frequency=0.3846;
                var frequency=0.5;
                var c = 0;

                var r =   Math.sin(frequency*c + 0) * (127) + 128;
                var g =   Math.sin(frequency*c + 2) * (127) + 128;
                var b =   Math.sin(frequency*c + 4) * (127) + 128;
                c++;

                var currentOrderGroup;

                for (var i=0; i < images.length && i < DIMENSION*DIMENSION; i++) {

                    var radius = Math.sqrt(i+2.5);
                    angle += Math.asin(1/radius);
                    var x = Math.cos(angle)*(radius*d);
                    var y = Math.sin(angle)*(radius*d);

                    if( orderBy !== undefined && orderBy != ''){
                        //orderBy = orderBy.toUpperCase();
                        //console.log('orderBy: '+orderBy);
                        if( images[i]['ORD'] === undefined  ){
                            orderBy = undefined;
                        }else{
                            if( currentOrderGroup === undefined ) currentOrderGroup = images[i]['ORD'];
                            if( currentOrderGroup != images[i]['ORD'] ){
                                currentOrderGroup = images[i]['ORD'];
                                r =   Math.sin(frequency*c + 0) * (127) + 128;
                                g =   Math.sin(frequency*c + 2) * (127) + 128;
                                b =   Math.sin(frequency*c + 4) * (127) + 128;
                                c++;
                            }
                        }
                    }



                    //console.log(color);


                    createImageObject(i,x,y,-1, 1, 0, new BABYLON.Color3( Math.floor(r)/255,Math.floor(g)/255,Math.floor(b)/255) );
                }
                $('#loadingScreen').css('display','none');
            }

            function getData( options ){
                if(options===undefined)options='';
                $('#loadingScreen').css('display','block');
                disposeImageData();
                var url = 'getImageData.php?'+orderBy+'&'+options;
                console.log(url);

                $.getJSON(url, function(data){
                    images = data;
                    loadImageData();
                });
            }

            function useSearchFilter(){
                var options = [];
                $('.div_search').each(function(){
                    if($(this).data('search_option_key') !== undefined){
                        for(var i = 0; i < $(this).data('search_option_key').length; i++){
                            var key = $(this).data('search_option_key')[i];
                            var value = $(this).data('search_option_value')[i];
                            options.push(key+"[]="+value);
                        }
                    }
                });


                console.log(''+options.join('&'));

                getData(options.join('&'));

            }


            function requestFullScreen(element) {
                // Supports most browsers and their versions.
                var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullscreen;

                if (requestMethod) { // Native full screen.
                    requestMethod.call(element);
                } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
                    var wscript = new ActiveXObject("WScript.Shell");
                    if (wscript !== null) {
                        wscript.SendKeys("{F11}");
                    }
                }
            }

            function createSearchFilter(){
                var div = $('<div/>');

                var last = $('.div_search').last();
                if( last !== undefined ){
                    var top = parseInt( last.css('top') );
                    top += 40;
                    //console.log(top);
                    div.css('top',top+'px');
                }

                div.addClass('div_search');


                var select = $('<select/>');

                select.click(function(event){
                    event.stopPropagation();
                });

                select.attr('name','search_type');
                select.attr('size','1');
                select.append($('<option/>').attr('value','none').text('-----'));
                select.append($('<option/>').attr('value','equal_reference,jahr').text('Jahr'));
                select.append($('<option/>').attr('value','between_reference,jahr').text('Jahr zwischen ...'));
                select.append($('<option/>').attr('value','equal_reference,ort').text('Ort'));
                select.append($('<option/>').attr('value','equal_reference,standort').text('Standort'));
                select.append($('<option/>').attr('value','equal_reference,technikthesaurus').text('Technikthesaurus'));
                select.append($('<option/>').attr('value','like,sik_invnr').text('Sik Invnr'));
                select.append($('<option/>').attr('value','like,kat_nr').text('Katalognummer'));
                select.append($('<option/>').attr('value','like,titel').text('Titel'));
                select.append($('<option/>').attr('value','like,repro_nachweis').text('Repronachweis'));
                select.append($('<option/>').attr('value','like,sikart_url').text('Sikart URL'));
                select.append($('<option/>').attr('value','between,masse_breite').text('Masse - Breite zwischen ...'));
                select.append($('<option/>').attr('value','between,masse_laenge').text('Masse - Länge zwischen ...'));
                select.append($('<option/>').attr('value','between,masse_hoehe').text('Masse - Höhe zwischen ...'));
                select.append($('<option/>').attr('value','equal_reference,technik').text('Technik'));

                var trimValue = function( value ){
                    if(value.length > 30)return value.substr(0,27)+'...';
                    return value;
                };


                select.change(function(){
                    var data = $(this).find("option:selected").attr('value').split(',');

                    var onFocus = function(){
                        blockInput = true;
                    };

                    var loseFocus = function(){
                        blockInput = false;
                    };

                    $(this).parent().find( "select[name!='search_type']").remove();
                    $(this).parent().find( "input[name!='search_type']").remove();
                    if( data[0] !== 'none'){

                        //remove additional fields



                        switch( data[0] ){

                            case 'equal_reference':
                                $.getJSON('getReferenceTable.php?table='+data[1], function(entries){
                                    //console.log(entries);

                                    var referenceSelect = $('<select/>');
                                    referenceSelect.click(function(event){
                                        event.stopPropagation();
                                    });
                                    referenceSelect.attr('name','reference');

                                    referenceSelect.append($('<option/>').attr('value','none').text('-----'));

                                    for( var i = 0; i < entries.length; i++ ){
                                        var value = entries[i][data[1].toUpperCase()];
                                        referenceSelect.append( $('<option/>').attr('value',value).text(trimValue(value)) );
                                    }
                                    referenceSelect.change(function(){
                                        div.data('search_option_key',[data[1]]);
                                        div.data('search_option_value',[referenceSelect.find("option:selected").attr('value')]);
                                        useSearchFilter();
                                    });
                                    select.after(referenceSelect);

                                });
                                break;

                            case 'between_reference':
                                $.getJSON('getReferenceTable.php?table='+data[1], function(entries){
                                    var referenceSelectFrom = $('<select/>');
                                    referenceSelectFrom.click(function(event){
                                        event.stopPropagation();
                                    });
                                    referenceSelectFrom.attr('name','reference_from');
                                    referenceSelectFrom.append($('<option/>').attr('value','none').text('-----'));
                                    var referenceSelectTo   = $('<select/>');
                                    referenceSelectTo.click(function(event){
                                        event.stopPropagation();
                                    });
                                    referenceSelectTo.attr('name','reference_to');
                                    referenceSelectTo.append($('<option/>').attr('value','none').text('-----'));

                                    for( var i = 0; i < entries.length; i++ ){
                                        var value = entries[i][data[1].toUpperCase()];
                                        referenceSelectFrom.append( $('<option/>').attr('value',value).text(trimValue(value)) );
                                        referenceSelectTo.append( $('<option/>').attr('value',value).text(trimValue(value)) );
                                    }

                                    var checkValues = function(){
                                        if(referenceSelectFrom.find("option:selected").attr('value') !== 'none' &&
                                           referenceSelectTo.find("option:selected").attr('value') !== 'none'){
                                            div.data('search_option_key',[data[1]+'_von',data[1]+'_bis']);
                                            var from = referenceSelectFrom.find("option:selected").attr('value');
                                            var to = referenceSelectTo.find("option:selected").attr('value');
                                            div.data('search_option_value',[from,to]);
                                            useSearchFilter();
                                        }
                                    };

                                    referenceSelectFrom.change(checkValues);
                                    referenceSelectTo.change(checkValues);


                                    select.after(referenceSelectTo);
                                    select.after(referenceSelectFrom);

                                });
                                break;

                            case 'between':

                                var textfieldFrom = $('<input/>').attr('type','text').attr('name','between_from');
                                var textfieldTo = $('<input/>').attr('type','text').attr('name','between_to');

                                textfieldFrom.click(function(event){
                                    event.stopPropagation();
                                });

                                textfieldTo.click(function(event){
                                    event.stopPropagation();
                                });

                                select.after(textfieldTo);
                                select.after(textfieldFrom);

                                var checkValues = function(e){
                                    if(e.which === 13){
                                        //console.log(textfieldFrom.val());
                                        if(textfieldFrom.val() !== '' && textfieldTo.val() !== '' ){
                                            div.data('search_option_key',[data[1]+'_von',data[1]+'_bis']);
                                            div.data('search_option_value',[textfieldFrom.val(),textfieldTo.val()]);
                                            useSearchFilter();
                                        }
                                    }
                                };


                                textfieldTo.focus(onFocus);
                                textfieldFrom.focus(onFocus);

                                textfieldTo.focusout(loseFocus);
                                textfieldFrom.focusout(loseFocus);

                                textfieldTo.keyup( checkValues );
                                textfieldFrom.keyup( checkValues );


                                break;

                            case 'like':
                                var textfield = $('<input/>').attr('type','text').attr('name','like');
                                textfield.click(function(event){
                                    event.stopPropagation();
                                });

                                textfield.focus(onFocus);

                                textfield.focusout(loseFocus);

                                textfield.keyup(function(e){

                                    if(e.which === 13) {
                                        div.data('search_option_key', [data[1]]);
                                        div.data('search_option_value', [textfield.val()]);
                                        useSearchFilter();
                                    }
                                });
                                select.after(textfield);
                                break;

                        }

                    }
                    //console.log(data);
                });

                div.append(select);

                var remButton = $("<img/>").attr('src','icon/minus.png').css('width','20px').css('heigth','20px');
                remButton.click(function(event){
                    event.stopPropagation();
                    div.nextAll().each(function(){
                        var top = parseInt( $(this).css('top') );
                        top -= 40;
                        $(this).css('top',top+'px');
                    });
                    div.remove();
                    var data = select.find("option:selected").attr('value').split(',');
                    if( data[0] !== 'none'){
                        useSearchFilter();
                    }
                });
                remButton.addClass('button_rem');
                select.before(remButton);

                return div;
            }


            engine.runRenderLoop(function(){
                scene.render();
            });

            window.addEventListener("resize",function(){
                engine.resize();
            });


        </script>
    </body>
</html>