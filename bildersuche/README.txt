Usage
------------------------------------------

1. Install a local server:
   Download xampp (https://www.apachefriends.org/download.html)
   
2. Put ivisWebGLSprites folder into htdocs

3. Open http://localhost/ivisWebGLSprites

4. Debug: Press F12 (or open "developer tools" manually, peferably in Chrome)
		  Go to "console", type in e.g. "group.children" to traverse javascript objects.
		  
		  
In case of emergency: roman.bolzern@fhnw.ch	
	
Enjoy!