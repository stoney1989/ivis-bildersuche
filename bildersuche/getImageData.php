<?php

    $db = new PDO('sqlite:image.db');
    $sql = 'SELECT i.* ';

    if(@$_GET['order_by']){
        $order = $_GET['order_by'];
        if( checkColumnExists($order, 'image') ){
            $sql .= ', '.$order.' as ORD ';
            $sql .= ' FROM image i ';
        }else{
            $_GET['order_by'] = 0;
        }
    }else if(@$_GET['order_by_reference']){
        $order = $_GET['order_by_reference'];
        if( checkColumnExists($order, $order) ){
            $sql .= ' , o.'.$order.' as ORD ';
            $sql .= ' FROM image i ';
            $sql .= ' LEFT JOIN '.$order.' o ON i.fk_'.$order.'=o.id ';
        }else{
            $_GET['order_by_reference'] = 0;
        }
    }else{
        $sql .= ' FROM image i ';
    }


    $conditions      = array();
    $conditionValues = array();


    function addLike( $key ){
        if( @$_GET[$key] ){
            global $conditions, $conditionValues;

            $tmp = array();
            for($i = 0; $i < count(@$_GET[$key]); $i++){
                $tmp[] = ' i.'.$key.' LIKE ? ';
                $conditionValues[] = '%'.$_GET[$key][$i].'%';
            }

            if( count($tmp) > 0 ){
                $conditions[] = '( '.join(' OR ',$tmp).' )';
            }


        }
    }

    function addBetweenReference( $key ){
        if( @$_GET[$key.'_von'] && @$_GET[$key.'_bis'] ){
            global $db, $conditions, $conditionValues;
            $stmt = $db->prepare('SELECT ID FROM '.$key.' WHERE '.$key.' BETWEEN ? AND ? COLLATE NOCASE');
            $stmt->execute(array(@$_GET[$key.'_von'][0],@$_GET[$key.'_bis'][0]));
            $result = $stmt->fetchAll(PDO::FETCH_NUM);
            if($result){
                $tmp = array();
                for( $i = 0; $i < count($result); $i++){
                    $conditionValues[] = $result[$i][0];
                    $tmp[] = ' i.fk_'.$key.' = ? ';
                }
                $conditions[] = '( '.join(' OR ',$tmp).' )';
            }
        }
    }

    function addBetween($key){
        if( @$_GET[$key.'_von'] && @$_GET[$key.'_bis']  ){
            global $conditions, $conditionValues;
            $conditions[] = ' i.'.$key.' BETWEEN ? AND ? ';
            $conditionValues[] = $_GET[$key.'_von'][0];
            $conditionValues[] = $_GET[$key.'_bis'][0];
        }
    }

    function addTechnikReference(){
        $key = 'technik';
        if( @$_GET[$key] ){
            global $db, $conditions, $conditionValues;

            $tmp = array();

            for($i = 0; $i < count(@$_GET[$key]); $i++){
                $stmt = $db->prepare('SELECT ID FROM '.$key.' WHERE '.$key.' = ? COLLATE NOCASE');
                $stmt->execute(array($_GET[$key][$i]));
                $result = $stmt->fetchAll(PDO::FETCH_NUM);
                if($result){
                    for($j = 0; $j < count($result); $j++){
                        $stmt2 = $db->prepare('SELECT fk_image FROM image_has_technik WHERE fk_technik  = ? COLLATE NOCASE');
                        $stmt2->execute(array($result[$j][0]));
                        $result2 = $stmt2->fetchAll(PDO::FETCH_NUM);
                        if($result2){
                            for($k = 0; $k < count($result2); $k++){
                                $conditionValues[] = $result2[$k][0];
                                $tmp[] = ' i.ID = ? ';
                            }
                        }
                    }
                    $conditions[] = '( '.join(' OR ',$tmp).' )';
                }
            }
        }
    }

    function addEqualReference( $key ){
        if( @$_GET[$key] ){
            global $db, $conditions, $conditionValues;

            $tmp = array();

            for($i = 0; $i < count(@$_GET[$key]); $i++){
                //print $_GET[$key][$i];
                $stmt = $db->prepare('SELECT ID FROM '.$key.' WHERE '.$key.' = ? COLLATE NOCASE');
                $stmt->execute(array($_GET[$key][$i]));
                $result = $stmt->fetchAll(PDO::FETCH_NUM);
                //print_r($result);
                if($result){
                    $tmp[] = ' i.fk_'.$key.'  = ? ';
                    $conditionValues[] = $result[0][0];
                }
            }

            if( count($tmp) > 0 ){
                $conditions[] = '( '.join(' OR ',$tmp).' )';
            }

        }
    }



    addEqualReference('jahr');
    addEqualReference('ort');
    addEqualReference('standort');
    addLike('sik_invnr');
    addLike('kat_nr');
    addBetweenReference('jahr');
    addBetween('masse_hoehe');
    addBetween('masse_laenge');
    addBetween('masse_breite');
    addLike('titel');
    addLike('repro_nachweis');
    addLike('sikart_url');
    addEqualReference('technikthesaurus');
    addTechnikReference();


    if( count($conditions) > 0 ){
        $sql .= ' WHERE ';
        $sql .= join(' AND ',$conditions);
    }

    if(@$_GET['order_by'] || @$_GET['order_by_reference']){
        $sql .= ' ORDER BY ORD';
        if(@$_GET['order_desc']){
            $sql .= ' DESC ';
        }else{
            $sql .= ' ASC ';
        }
    }


    function checkTableExists( $table ){
        $table = strtolower( $table );
        global $db;
        $found = 0;
        $stmt = $db->prepare("SELECT name FROM sqlite_master WHERE type='table'");
        $stmt->execute();
        while($found == 0 && $data = $stmt->fetch( PDO::FETCH_ASSOC )){
            if($data['name'] == $table){
                $found = 1;
            }
        }
        return $found;
    }


    function checkColumnExists( $column, $table ){
        $found = 0;
        if( checkTableExists( $table ) ){
            $column = strtoupper( $column );
            global $db;

            $stmt = $db->prepare('PRAGMA table_info( '.$table.' )');
            $stmt->execute();
            while($found == 0 && $data = $stmt->fetch( PDO::FETCH_ASSOC )){
                if($data['name'] == $column){
                    $found = 1;
                }
            }
        }
        return $found;
    }

    //print $sql;
    //print_r($conditionValues);
    $stmt = $db->prepare($sql);

    if (!$stmt) {
        print "\nPDO::errorInfo():\n";
        print_r($db->errorInfo());
    }

    $stmt->execute($conditionValues);

    print json_encode($stmt->fetchAll( PDO::FETCH_ASSOC ));

    $db = null;

    //while($data = $stmt->fetch( PDO::FETCH_ASSOC )){
    //    print json_encode($data).'<br/>';
    //}
?>