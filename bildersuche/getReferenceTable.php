<?php
    if(@$_GET['table']){
        $db = new PDO('sqlite:image.db');
        $table = filter($_GET['table']);
        $stmt = $db->prepare("select * from ".$table." order by ".$table);
        if (!$stmt) {
            print "\nPDO::errorInfo():\n";
            print_r($db->errorInfo());
        }
        $stmt->execute();

    }

    function filter( $table ){
        if( $table != 'jahr' && $table != 'ort' && $table != 'standort' && $table != 'technikthesaurus' && $table != 'technik')return 'jahr';
        return $table;
    }

    print json_encode($stmt->fetchAll( PDO::FETCH_ASSOC ));

    $db = null;


?>