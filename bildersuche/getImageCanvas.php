<?php


    header("Content-Type: image/jpeg");

    $cache=1;
    if( @$_GET["no_cache"] )$cache=0;

    if($cache)$canvas = @imagecreatefromjpeg('image/canvas.jpeg');

    if( !$cache || !$canvas){
        $db = new PDO('sqlite:image.db');
        $stmt = $db->prepare("select * from image order by id limit 512");
        $stmt->execute();

        $DIMENSION = 128;
        $IMAGES_PER_LINE = 32;

        $canvas = imagecreatetruecolor( 4096, 2048 );

        while($row = $stmt->fetch( PDO::FETCH_NUM )){

            $index = $row[0];
            $x = $index%$IMAGES_PER_LINE;
            $y = floor($index/$IMAGES_PER_LINE);

            //echo $x." ".$y."<br/>";

            $image = @ImageCreateFromJPEG ( 'image/'.$row[14].'.jpg' );

            if(!$image){
                $image = @ImageCreateFromJPEG ( 'image/94309D1.jpg');
            }

            if($image){
                $w = imagesx($image);
                $h = imagesy($image);
                imagecopyresampled ( $canvas, $image , $x*$DIMENSION , $y*$DIMENSION , 0 , 0 , $DIMENSION , $DIMENSION , $w , $h );
                $color = $color = imageColorResolveAlpha($canvas, 0,0,0,0);
                imagesetthickness ( $canvas , 4 );
                imagerectangle( $canvas,  $x*$DIMENSION+4,  $y*$DIMENSION+4, $x*$DIMENSION+$DIMENSION-4, $y*$DIMENSION+$DIMENSION-4, $color );
            }
//            $x++;
//            if($x >= $IMAGES_PER_LINE){
//                $x = 0;
//                $y++;
//            }
        }
        $db = null;
        if($cache)imagejpeg($canvas, 'image/canvas.jpeg');
    }

    imagejpeg($canvas);



//    ob_start ();
//
//    imagejpeg($canvas);
//    $image_data = ob_get_contents ();
//
//    ob_end_clean ();
//
//    $image_data_base64 = base64_encode ($image_data);
//
//    print urlencode($image_data_base64);

    imagedestroy($canvas);
?>